import 'package:firebase_database/firebase_database.dart';
import 'package:parcial3/models/avion.dart';


class AvionDao {
  final DatabaseReference _avionRef = FirebaseDatabase.instance.reference().child('avion');

  void saveAvion(Avion avion) {
    _avionRef.push().set(avion.toJson());
  }

  Query getAvionesQuery() {
    return _avionRef;
  }

  
}