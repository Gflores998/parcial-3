///
/// Hecho: Gerardo Alexis Flores Mejia
/// Carnet: 25-0036-2017
/// Carrera: Ingeniería en Sistemas
///
import 'package:flutter/material.dart';
import 'package:parcial3/pages/aviones_page.dart';
import 'package:parcial3/pages/clientes_page.dart';
import 'package:parcial3/pages/destinos_page.dart';
import 'package:parcial3/pages/horarios_page.dart';
import 'package:parcial3/pages/reservas_page.dart';
import 'package:parcial3/pages/vuelos_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var pageList = [
    AvionesPage(),
    ClientesPage(),
    DestinosPage(),
    HorariosPage(),
    ReservasPage(),
    VuelosPage()
  ];

  int indexPage = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60.0),
          child: AppBar(
            centerTitle: true,
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text('Inicio'),
          ),
        ),
        body: pageList[indexPage],
        bottomNavigationBar: BottomNavigationBar(
            iconSize: 20,
            type: BottomNavigationBarType.fixed,
            showUnselectedLabels: true,
            selectedFontSize: 10,
            unselectedFontSize: 10,
            currentIndex: indexPage,
            onTap: (int currentIndex) {
              setState(() {
                indexPage = currentIndex;
              });
            },
            items: [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Aviones'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.star), label: 'Clientes'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.notifications), label: 'Destino'),
              BottomNavigationBarItem(
                  icon: Icon(Icons.mail), label: 'Horario'),
              BottomNavigationBarItem(icon: Icon(Icons.search), label: 'Reserva'),
              BottomNavigationBarItem(icon: Icon(Icons.pedal_bike), label: 'Vuelos')
            ]));
  }
}
