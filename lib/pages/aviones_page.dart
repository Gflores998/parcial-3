/// 
/// Hecho: Gerardo Alexis Flores Mejia
/// Carnet: 25-0036-2017
/// Carrera: Ingeniería en Sistemas
///
import 'package:flutter/material.dart';
import 'package:parcial3/dao/avion_dao.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';


class AvionesPage extends StatefulWidget {
  const AvionesPage({ Key? key }) : super(key: key);

  @override
  _AvionesPageState createState() => _AvionesPageState();
}

class _AvionesPageState extends State<AvionesPage> {

  final avionDao = AvionDao();

  final nameCtrl = TextEditingController();
  final statusCtrl = TextEditingController();

  
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: SafeArea(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox( height: 10 )
          ],),
        )
      ),
    );
  }
}