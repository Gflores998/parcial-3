class Vuelo {
  final int idVuelo;
  final String disponibilidad;
  final String tipo_vuelo;
  final String avion_codigo;
  final int destinos_id_destino;

  Vuelo(this.idVuelo, this.disponibilidad, this.tipo_vuelo, this.avion_codigo, this.destinos_id_destino);

  Vuelo.fromJson(Map<dynamic, dynamic> json)
  : idVuelo = json['idVuelo'] as int,
  disponibilidad = json['disponibilidad'] as String,
  tipo_vuelo = json['tipo_vuelo'] as String,
  avion_codigo = json['avion_codigo'] as String,
  destinos_id_destino = json['destinos_id_destino'] as int;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
      'idVuelo': idVuelo,
      'disponibilidad': disponibilidad,
      'tipo_vuelo': tipo_vuelo,
      'avion_codigo': avion_codigo,
      'destinos_id_destino': destinos_id_destino
    };


}