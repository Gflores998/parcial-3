class Reserva {
  final int idReservas;
  final String estado;
  final int vuelos_idVuelo;

  Reserva(this.idReservas, this.estado, this.vuelos_idVuelo);

  Reserva.fromJson(Map<dynamic, dynamic> json)
  : idReservas = json['idReservas'] as int,
  estado = json['estado'] as String,
  vuelos_idVuelo = json['vuelos_idVuelo'] as int;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
      'idReservas': idReservas,
      'estado': estado,
      'vuelos_idVuelo': vuelos_idVuelo
    };
  
}