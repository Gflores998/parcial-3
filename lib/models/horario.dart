class Horario {
  final int id_horario;
  final String hora_vuelo;

  Horario(this.id_horario, this.hora_vuelo);

  Horario.fromJson(Map<dynamic, dynamic> json)
  : id_horario = json['id_horario'] as int,
  hora_vuelo = json['hora_vuelo'] as String;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
      'id_horario': id_horario,
      'hora_vuelo': hora_vuelo
    };

}