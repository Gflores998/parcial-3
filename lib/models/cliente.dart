

class Cliente {
  final int cedula;
  final String nombre;
  final String apellido;
  final DateTime fecha_nacimiento;
  final String sexo;
  final String tipo;
  final String usuario;
  final int reservas_idReservas;

  Cliente(this.cedula, this.nombre, this.apellido, this.fecha_nacimiento, this.sexo, this.tipo, this.usuario, this.reservas_idReservas);

  Cliente.fromJson(Map<dynamic, dynamic> json)
  : cedula = json['cedula'] as int,
  nombre = json['nombre'] as String,
  apellido = json['apellido'] as String,
  fecha_nacimiento = DateTime.parse(json['fecha_nacimiento'] as String),
  sexo = json['sexo'] as String,
  tipo = json['tipo'] as String,
  usuario = json['usuario'] as String,
  reservas_idReservas = json['reservas_idReservas'] as int;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
      'cedula': cedula,
      'nombre': nombre,
      'apellido': apellido,
      'fecha_nacimiento': fecha_nacimiento.toString(),
      'sexo': sexo,
      'tipo': tipo,
      'usuario': usuario,
      'reservas_idReservas': reservas_idReservas,
    };


}