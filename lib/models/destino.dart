class Destino {
  final int id_destino;
  final String nombre;
  final String horarios_id_horario;

  Destino(this.id_destino, this.nombre, this.horarios_id_horario);

  Destino.fromJson(Map<dynamic, dynamic> json)
  : id_destino = json['id_destino'] as int,
  nombre = json['nombre'] as String,
  horarios_id_horario = json['horarios_id_horario'] as String;

  Map<dynamic, dynamic> toJson() => <dynamic, dynamic>{
      'id_destino': id_destino,
      'nombre': nombre,
      'horarios_id_horario': horarios_id_horario,
    };

}